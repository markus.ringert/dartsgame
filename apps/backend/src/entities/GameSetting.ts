import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { GameType } from './GameType';
import { Game } from './Game';

@Entity()
export class GameSetting {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @OneToOne(() => Game, (game) => game.id)
    game: Game

    @ManyToOne(() => GameType, (gameType) => gameType.id)
    gameType: GameType

    @Column({ type: 'json', nullable: true, default: null })
    rules: X01 | Shanghai
}