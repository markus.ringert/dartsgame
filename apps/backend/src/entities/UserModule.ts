import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './User';
import { GameType } from './GameType';
import { GameSetting } from './GameSetting';
import { Game } from './Game';
import { Field } from './Field';
import { Throw } from './Throw';
import { Recording } from './Recording';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      GameType,
      GameSetting,
      Game,
      Field,
      Throw,
      Recording
    ]),
  ]
})
export class EntitiesModule {}
