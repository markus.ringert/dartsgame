import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { Recording } from './Recording';
import { Field } from './Field';

@Entity()
export class Throw {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne(() => Recording, (recording) => recording.id)
    recording: Recording

    @ManyToOne(() => Field, (field) => field.id)
    field: Field;

    @Column()
    points: number;
}