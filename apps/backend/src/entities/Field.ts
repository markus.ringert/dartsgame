import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Field {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    fieldName: string;
}