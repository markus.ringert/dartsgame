import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { User } from './User';
import { GameSetting } from './GameSetting';

@Entity()
export class Game {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @OneToOne(() => GameSetting, (gameSetting) => gameSetting.id)
    gameSetting: GameSetting

    @ManyToMany(() => User)
    @JoinTable()
    player: User[]
}