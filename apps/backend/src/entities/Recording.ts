import { Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Game } from './Game';
import { User } from './User';

@Entity()
export class Recording {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne(() => Game, (game) => game.id)
    game: Game

    @ManyToOne(() => User, (user) => user.id)
    player: User
}