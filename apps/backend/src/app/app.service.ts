import { Injectable } from '@nestjs/common';
import { UsersService } from '../domains/user/services/UserService';
import { User } from '../entities/User';
import { AuthService } from '../domains/authentication/services/AuthService';

@Injectable()
export class AppService {

  constructor(private userService: UsersService, private authService: AuthService) {}

  async getData(): Promise<{ message: string }> {
    let user: User = await this.userService.findByUsername('MarkuDerEchte');

    return { message: `Hallo von ${user?.username}!` };
  }
}
