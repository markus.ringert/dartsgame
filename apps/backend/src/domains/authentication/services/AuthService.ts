import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from '../dto/CreateUserDto';
import { UsersService } from '../../user/services/UserService';
import { AuthDto } from '../dto/AuthDto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  static readonly ERROR_USER_DOES_NOT_EXISTS = 'USER_DOES_NOT_EXISTS';
  static readonly ERROR_USER_ALREADY_EXISTS = 'USER_ALREADY_EXISTS';

  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private config: ConfigService
  ) {}

  public async signUp(createUserDto: CreateUserDto): Promise<{
    accessToken: string;
    refreshToken: string;
  }> {
    // Check if user exists
    const userExists = await this.usersService.findByUsername(
      createUserDto.username
    );
    if (userExists) {
      throw new BadRequestException(AuthService.ERROR_USER_ALREADY_EXISTS);
    }

    // Hash password
    const hash = await this._hashData(createUserDto.password);
    const newUser = await this.usersService.create({
      ...createUserDto,
      password: hash,
    });
    
    const tokens = await this._getTokens(newUser.id, newUser.username);
    await this._updateRefreshToken(newUser.id, tokens.refreshToken);
    return tokens;
  }

  public async signIn(data: AuthDto) {
    // Check if user exists
    const user = await this.usersService.findByUsername(data.username);
    if (!user) {
      throw new BadRequestException(AuthService.ERROR_USER_DOES_NOT_EXISTS);
    }

    const passwordMatches = await this._verify(data.password, user.password);
    if (!passwordMatches) {
      throw new BadRequestException(AuthService.ERROR_USER_DOES_NOT_EXISTS);
    }

    const tokens = await this._getTokens(user.id, user.username);
    await this._updateRefreshToken(user.id, tokens.refreshToken);
    return tokens;
  }

  public async logout(userId: string) {
    return this.usersService.update(userId, { refreshToken: null });
  }

  private async _hashData(data: string): Promise<string> {
    return bcrypt.hash(
      data,
      await bcrypt.genSalt(parseInt(this.config.get<string>('SALT_ROUNDS')))
    );
  }

  private async _verify(password: string, dbPassword: string): Promise<boolean> {
    return await bcrypt.compare(password, dbPassword);
  }

  private async _updateRefreshToken(userId: string, refreshToken: string) {
    const hashedRefreshToken = await this._hashData(refreshToken);
    await this.usersService.update(userId, {
      refreshToken: hashedRefreshToken,
    });
  }

  private async _getTokens(userId: string, username: string) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
        },
        {
          secret: this.config.get<string>('JWT_ACCESS_SECRET'),
          expiresIn: '15m',
        }
      ),
      this.jwtService.signAsync(
        {
          sub: userId,
          username,
        },
        {
          secret: this.config.get<string>('JWT_REFRESH_SECRET'),
          expiresIn: '7d',
        }
      ),
    ]);

    return {
      accessToken,
      refreshToken,
    };
  }
}
