import {
  BadRequestException,
  Body,
  Controller,
  InternalServerErrorException,
  Post
} from '@nestjs/common';

import { AuthService } from '../services/AuthService';
import { CreateUserDto } from '../dto/CreateUserDto';

@Controller()
export class SignupController {
  constructor(private readonly authService: AuthService) {}

  @Post('/auth/signup')
  async signup(@Body() createUserDto: CreateUserDto) {
    try {
      let token = await this.authService.signUp(createUserDto);

      return token;
    } catch (e) {
      switch (e.message) {
        case AuthService.ERROR_USER_ALREADY_EXISTS:
          throw new BadRequestException(e.message, {
            cause: e,
            description: 'Der Nutzername wird bereits genutzt.',
          });
        default:
          // TODO: log error
          throw new InternalServerErrorException('INTERNAL_ERROR');
      }
    }
  }
}
