import {
  BadRequestException,
  Body,
  Controller,
  InternalServerErrorException,
  Post
} from '@nestjs/common';

import { AuthService } from '../services/AuthService';
import { AuthDto } from '../dto/AuthDto';

@Controller()
export class SigninController {
  constructor(private readonly authService: AuthService) {}

  @Post('/auth/signin')
  async signin(@Body() authDto: AuthDto) {
    try {
      let token = await this.authService.signIn(authDto);

      return token;
    } catch (e) {
      switch (e.message) {
        case AuthService.ERROR_USER_DOES_NOT_EXISTS:
          throw new BadRequestException(e.message, {
            cause: e,
            description: 'Nutzername oder Passwort falsch.',
          });
        default:
          // TODO: log error
          throw new InternalServerErrorException('INTERNAL_ERROR');
      }
    }
  }
}
