import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../entities/User';
import { AccessTokenStrategy } from './strategies/accessToken';
import { RefreshTokenStrategy } from './strategies/refresh';
import { AuthService } from './services/AuthService';
import { JwtService } from '@nestjs/jwt';
import { SignupController } from './controller/signup.controller';
import { SigninController } from './controller/signin.controller';
import { UsersModule } from '../user/UserModule';

@Module({
  imports: [UsersModule],
  providers: [
    AuthService,
    JwtService,
    AccessTokenStrategy,
    RefreshTokenStrategy
  ],
  controllers: [SignupController, SigninController],
  exports: [AuthService],
})
export class AuthenticationModule {}
