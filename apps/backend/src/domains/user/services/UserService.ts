import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../../entities/User';
import { CreateUserDto } from '../../authentication/dto/CreateUserDto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {}

  public async findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  public async findOne(id: string): Promise<User | null> {
    return this.usersRepository.findOneBy({ id });
  }

  public async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  public async findByUsername(username: string): Promise<User | null> {
    return this.usersRepository.findOneBy({
      username,
    });
  }

  public async create(createUserDto: CreateUserDto): Promise<User> {
    return this.usersRepository.save(
      await this.usersRepository.create({
        username: createUserDto.username,
        password: createUserDto.password,
      })
    );
  }

  public async update(userId: string, updateDto: Partial<CreateUserDto>) {
    return this.usersRepository.update(
      { id: userId },
      {
        refreshToken: updateDto?.refreshToken,
      }
    );
  }
}
