import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './services/UserService';
import { User } from '../../entities/User';
import { UserController } from './controller/user.controller';
import { Game } from '../../entities/Game';
import { GameSetting } from '../../entities/GameSetting';
import { GameType } from '../../entities/GameType';
import { Field } from '../../entities/Field';
import { Throw } from '../../entities/Throw';
import { Recording } from '../../entities/Recording';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      GameType,
      GameSetting,
      Game,
      Field,
      Throw,
      Recording
    ]),
  ],
  providers: [
    UsersService,
  ],
  controllers: [UserController],
  exports: [UsersService]
})
export class UsersModule {}
