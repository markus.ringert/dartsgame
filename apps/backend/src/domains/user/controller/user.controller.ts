import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { UsersService } from '../services/UserService';
import { UserInfoDto } from '../dto/UserInfo';
import { AccessTokenGuard } from '../../authentication/guards/accessToken.guard';

@Controller()
export class UserController {
  constructor(private readonly userService: UsersService) {}

  @UseGuards(AccessTokenGuard)
  @Get('/user/info')
  async userInfo(@Req() req: Request): Promise<UserInfoDto> {
    try {
      let user = await this.userService.findOne(req.user!['sub']);

      return { username: user.username };
    } catch (e) {
      switch (e.message) {
        default:
          // TODO: log error
          throw new InternalServerErrorException('INTERNAL_ERROR');
      }
    }
  }
}
