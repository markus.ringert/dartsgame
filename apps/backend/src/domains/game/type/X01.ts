interface X01 {
    doubleIn: boolean;
    doubleOut: boolean;
    points: number;
    bestOfLegs: number;
    bestOfSets: number;
}