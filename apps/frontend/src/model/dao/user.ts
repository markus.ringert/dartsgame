import { signal, WritableSignal } from "@angular/core";

export class User {
    constructor(
        private _id: WritableSignal<string>,
        private _username: WritableSignal<string|null> = signal(null)
    ) {
    }

    public set username(username: string) {
        this._username.set(username);
    }

    public get id(): string {
        return this._id();
    }

    public get username(): string | null {
        return this._username();
    }
}
