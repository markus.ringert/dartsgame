import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';
import { appRoutes } from './app.routes';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { LocalDataModule } from '../domains/localData';
import { accessTokenInterceptor } from '../domains/authentication';
import { UserModule } from '../domains/user/user.module';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(appRoutes),
    provideHttpClient(withInterceptors([accessTokenInterceptor])),
    importProvidersFrom(LocalDataModule),
    importProvidersFrom(UserModule)
  ],
};
