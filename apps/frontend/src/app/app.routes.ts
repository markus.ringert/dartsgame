import { Route } from '@angular/router';

export const appRoutes: Route[] = [
    {
        path: 'auth',
        children: [
            {
                title: 'SignUp',
                path: 'signup',
                loadComponent: () => import('../domains/authentication/signup/signup.component').then(m => m.SignupComponent)
            },
            {
                title: 'SignIn',
                path: 'signin',
                loadComponent: () => import('../domains/authentication/signin/signin.component').then(m => m.SigninComponent)
            }
        ]
    },
    {
        title: 'Dashboard',
        path: 'dashboard',
        loadComponent: () => import('../domains/dashboard/dashboard/dashboard.component').then(m => m.DashboardComponent)
    },
];
