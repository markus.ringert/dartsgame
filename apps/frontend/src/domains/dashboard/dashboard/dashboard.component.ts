import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { UserService } from '../../user/services/user.service';
import { UserModule } from '../../user/user.module';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    CommonModule,
    UserModule
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class DashboardComponent {
  public username: Observable<{username:string}>;

  constructor(
    userService: UserService
  ) {
    this.username = userService.userInfo();
  }
}
