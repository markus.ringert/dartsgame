import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'auth-layout-base-page',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './base-page.component.html',
  styleUrl: './base-page.component.css',
})
export class BasePageComponent {
    @Input() public title: string = '';
}
