import { NgModule } from '@angular/core';
import { SigninService } from './services/signin.service';
import { SignupService } from './services/signup.service';
import { UtilsModule } from '../utils/utils.module';

@NgModule({ imports: [UtilsModule], providers: [
        SigninService,
        SignupService
    ] })
export class AuthenticationModule {}
