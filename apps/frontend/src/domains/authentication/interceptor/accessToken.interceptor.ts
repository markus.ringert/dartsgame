import { HttpEvent, HttpHandlerFn, HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { inject } from '@angular/core';
import { UserService } from '../../user/services/user.service';
import { Observable, switchMap } from 'rxjs';


export const accessTokenInterceptor: HttpInterceptorFn = (req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> => {
  const userService = inject(UserService);
  const userId = req.headers.get('USER_ID');

  if (userId) {
    return userService.getAccessToken(userId).pipe(
      switchMap((accessToken) => {

        const authReq = req.clone({
          headers: req.headers.set('Authorization', `Bearer  ${accessToken!}`),
        });

        return next(authReq);
      })
    );
  }

  return next(req);
};
