import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { BasePageComponent } from '../layouts/base-page/base-page.component';
import { SignupService } from '../services/signup.service';
import { AuthenticationModule } from '../authentication.module';

@Component({
  selector: 'app-signup',
  standalone: true,
  imports: [
    CommonModule,
    BasePageComponent,
    ReactiveFormsModule,
    AuthenticationModule
  ],
  providers: [SignupService],
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.scss',
})
export class SignupComponent {
  public registrationForm!: FormGroup;
  public showErrors: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private signupService: SignupService
  ) {}

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      passwords: this.formBuilder.group({
        password: ['', Validators.required], // TODO: password requirements like 8 chars 1 digit etc.
        confirmPassword: ['', [Validators.required]],
      }, {validators: [this.passwordConfirming]}),
      terms: ['', [Validators.requiredTrue]],
    });

    this.registrationForm.valueChanges.subscribe(
      () => this.showErrors = false
    );
  }

  public submitForm(): void {
    this.showErrors = true;

    if (this.registrationForm.valid) {
      this.signupService.signup(
        this.registrationForm.get('username')?.value,
        this.registrationForm.get('email')?.value,
        this.registrationForm.get('passwords')?.get('password')?.value,
      ).subscribe(); // TODO: Error Handling & routing to signin
    }
  }

  public passwordConfirming(c: AbstractControl): { invalid: boolean } | null {
    if (c.get('password')?.value !== c.get('confirmPassword')?.value) {
        return {invalid: true};
    }

    return null;
  }
}
