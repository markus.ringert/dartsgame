import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../../utils';

@Injectable()
export class SigninService {
  constructor(
    private httpClient: HttpClient,
    private configService: ConfigService
  ) {}

  public signin(
    username: string,
    password: string
  ): Observable<{
    accessToken: string;
    refreshToken: string;
  }> {
    return this.httpClient.post<{
      accessToken: string;
      refreshToken: string;
    }>(this.configService.getApiUrl() + '/auth/signin', {
      username,
      password, // TODO: hash password before send
    });
  }
}
