import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from '../../utils';

@Injectable()
export class SignupService {

  constructor(
        private httpClient: HttpClient,
        private configService: ConfigService
  ) { }


  public signup(
    username: string,
    email: string,
    password: string
  ): Observable<{
    accessToken: string;
    refreshToken: string;
  }> {
    return this.httpClient.post<{
        accessToken: string;
        refreshToken: string;
      }>(this.configService.getApiUrl() + '/auth/signup', {
        username,
        email,
        password // TODO: hash password before send
    });
  }
}
