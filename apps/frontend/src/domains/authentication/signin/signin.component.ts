import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { BasePageComponent } from '../layouts/base-page/base-page.component';
import { SigninService } from '../services/signin.service';
import { Router } from '@angular/router';
import { UserService } from '../../user/services/user.service';
import { jwtDecode } from "jwt-decode";
import { AuthenticationModule } from '../authentication.module';
import { UserModule } from '../../user/user.module';

@Component({
  selector: 'app-signin',
  standalone: true,
  imports: [
    CommonModule,
    BasePageComponent,
    ReactiveFormsModule,
    AuthenticationModule,
    UserModule
  ],
  providers: [SigninService],
  templateUrl: './signin.component.html',
  styleUrl: './signin.component.scss',
})
export class SigninComponent {
  public loginForm!: FormGroup;
  public showErrors: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private signinService: SigninService,
    private router: Router,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required]
    });

    this.loginForm.valueChanges.subscribe(
      () => this.showErrors = false
    );
  }

  public submitForm(): void {
    this.showErrors = true;

    if (this.loginForm.valid) {
      this.signinService.signin(
        this.loginForm.get('username')?.value,
        this.loginForm.get('password')?.value,
      ).subscribe(
        (tokens) => {
          const userId = jwtDecode(tokens.accessToken).sub!;

          this.userService.setCurrentUserId(userId);
          this.userService.setAccessToken(userId, tokens.accessToken);
          this.userService.setRefreshToken(userId, tokens.refreshToken);

          this.router.navigateByUrl('/dashboard');
        }
      ); // TODO: Error Handling
    }
  }
}
