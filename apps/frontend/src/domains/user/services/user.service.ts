import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'apps/frontend/src/model/dao/user';
import { BehaviorSubject, filter, Observable, switchMap } from 'rxjs';
import { UserRepositoryFactory } from '../repository/userRepository.factory';
import { ConfigService } from '../../utils';
import { LocalDataService } from '../../localData';

@Injectable()
export class UserService {
  private _users: { [key: string]: BehaviorSubject<User | null> } = {};

  constructor(
    private _httpClient: HttpClient,
    private _repositoryFactory: UserRepositoryFactory,
    private _configService: ConfigService,
    private _dataService: LocalDataService
  ) {}

  public getCurrentUser(): Observable<User> {
    return this._dataService.getData('currentUserId').pipe(
      switchMap((currentUserId: string | undefined) => {
        return this.getUser(currentUserId!);
      })
    );
  }

  public getAccessToken(userId: string): Observable<string | undefined> {
    return this._dataService.getData(`accessToken_${userId}`);
  }

  public getRefreshToken(userId: string): Observable<string | undefined> {
    return this._dataService.getData(`refreshToken_${userId}`);
  }

  public setCurrentUserId(id?: string): void {
    this._dataService.setData('currentUserId', id);
  }

  public setAccessToken(userId: string, accessToken?: string): void {
    this._dataService.setData(`accessToken_${userId}`, accessToken);
  }

  public setRefreshToken(userId: string, refreshToken?: string): void {
    this._dataService.setData(`refreshToken_${userId}`, refreshToken);
  }

  public userInfo(): Observable<{ username: string }> {
    return this._dataService.getData('currentUserId').pipe(
      switchMap((currentUserId: string | undefined) => {
        const headers = new HttpHeaders({
          'USER_ID': currentUserId!
        });

        return this._httpClient.get<{
          username: string;
        }>(
          this._configService.getApiUrl() + '/user/info', {headers}
        );
      })
    );
  }

  public getUser(id: string): Observable<User> {
    let userSubject = this._users[id];

    if (userSubject) {
      return userSubject.pipe(filter((value) => value !== null)) as Observable<User>;
    }

    const userRepository = this._repositoryFactory.getRepository();

    userSubject = new BehaviorSubject<User | null>(null);
    userRepository.getUser(id).subscribe(userSubject);

    this._users[id] = userSubject;

    return this._users[id].pipe(filter((value) => value !== null)) as Observable<User>;
  }

  public setUser(user: User): void {
    const userSubject = this._users[user.id];

    if (userSubject) {
      userSubject.next(user);
    }

    this._users[user.id] = new BehaviorSubject<User | null>(user);
  }
}
