import { User } from 'apps/frontend/src/model/dao/user';
import { UserRepository } from './userRepository';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserRepositoryHttp implements UserRepository {
  constructor(private httpClient: HttpClient) {}

  public isSupported(): boolean {
    return true;
  }

  public getUser(id: string): Observable<User> {
    return this.httpClient.get<User>('userURL'); //TODO: BE!! 
  }
}
