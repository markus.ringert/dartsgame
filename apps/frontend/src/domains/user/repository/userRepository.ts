import { User } from "apps/frontend/src/model/dao/user";
import { Observable } from "rxjs";

export interface UserRepository {
    getUser(id: string): Observable<User>;
    isSupported(): boolean;
}