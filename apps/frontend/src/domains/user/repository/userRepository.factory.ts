import { Inject, Injectable, InjectionToken } from '@angular/core';
import { UserRepository } from './userRepository';

export const USER_REPOSITORY_INJECTION_TOKEN = new InjectionToken<UserRepository>(
  'USER_REPOSITORY_INJECTION_TOKEN'
);

@Injectable()
export class UserRepositoryFactory {
  constructor(
    @Inject(USER_REPOSITORY_INJECTION_TOKEN)
    private _userRepositories: Array<UserRepository>
  ) {}

  public getRepository(): UserRepository {
    const userRepository = this._userRepositories.find((userRepository) =>
      userRepository.isSupported()
    );

    if (userRepository) {
      return userRepository;
    }

    throw Error('No User Repository found!');
  }
}
