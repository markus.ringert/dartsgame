import { NgModule } from '@angular/core';
import { UserService } from './services/user.service';
import { UserRepositoryHttp } from './repository/userRepositoryHttp';
import { USER_REPOSITORY_INJECTION_TOKEN, UserRepositoryFactory } from './repository/userRepository.factory';
import { UtilsModule } from '../utils/utils.module';

@NgModule({ imports: [UtilsModule], providers: [
        UserService,
        UserRepositoryFactory,
        {
            provide: USER_REPOSITORY_INJECTION_TOKEN,
            useClass: UserRepositoryHttp,
            multi: true,
        },
    ] })
export class UserModule {}
