import { NgModule } from '@angular/core';
import { ConfigService } from './services/config.service';

@NgModule({
  providers: [
    ConfigService
  ],
})
export class UtilsModule {}
