import { Injectable } from '@angular/core';
import * as appConfig from '../../../env.json';

@Injectable()
export class ConfigService {
  public getApiUrl(): string {
    return appConfig.apiUrl;
  }
}
