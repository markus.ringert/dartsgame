import { NgModule } from '@angular/core';
import { LocalDataService } from './services/localData.service';
import { LOCAL_DATA_INJECTION_TOKEN, LocalDataRepositoryFactory } from './repository/localData.factory';
import { LocalDataRepositoryLocalStorage } from './repository/localDataRepositoryLocalStore';

@NgModule({
  providers: [
    LocalDataService,
    LocalDataRepositoryFactory,
    {
        provide: LOCAL_DATA_INJECTION_TOKEN,
        useClass: LocalDataRepositoryLocalStorage,
        multi: true,
    },
  ],
})
export class LocalDataModule {}
