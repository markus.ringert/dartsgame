import { Injectable } from '@angular/core';
import { LocalDataRepositoryFactory } from '../repository/localData.factory';
import { LocalDataRepository } from '../repository/localDataRepository';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {
  private _repository: LocalDataRepository;

  constructor(
    private _repositoryFactory: LocalDataRepositoryFactory
  ) {
    this._repository = this._repositoryFactory.getRepository();
  }

  public setData(key: string, data?: string): void {
    this._repository.set(key, data);
  }

  public removeData(key: string): void {
    this._repository.remove(key);
  }

  public getData(key: string): Observable<string | undefined> {
    return this._repository.get(key);
  }
}
