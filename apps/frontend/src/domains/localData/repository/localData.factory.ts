import { Inject, Injectable, InjectionToken } from '@angular/core';
import { LocalDataRepository } from './localDataRepository';

export const LOCAL_DATA_INJECTION_TOKEN = new InjectionToken<LocalDataRepository>(
  'LOCAL_DATA_INJECTION_TOKEN'
);

@Injectable()
export class LocalDataRepositoryFactory {
  constructor(
    @Inject(LOCAL_DATA_INJECTION_TOKEN)
    private _userRepositories: Array<LocalDataRepository>
  ) {}

  public getRepository(): LocalDataRepository {
    const repository = this._userRepositories.find((repository) =>
      repository.isSupported()
    );

    if (repository) {
      return repository;
    }

    throw Error('No Local Data Repository found!');
  }
}
