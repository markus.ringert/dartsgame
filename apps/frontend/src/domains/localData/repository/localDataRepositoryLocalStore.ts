import { LocalDataRepository } from './localDataRepository';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class LocalDataRepositoryLocalStorage implements LocalDataRepository {
  private _data: { [key: string]: BehaviorSubject<string | undefined> } = {};

  public isSupported(): boolean {
    return true;
  }

  public get(key: string): Observable<string | undefined> {
    this._checkNeedCreate(key);

    return this._data[key].asObservable();
  }

  public set(key: string, data: string): void {
    this._checkNeedCreate(key);

    localStorage.setItem(key, data);

    this._data[key].next(data);
  }

  public remove(key: string): void {
    this._checkNeedCreate(key);

    localStorage.removeItem(key);

    this._data[key].next(undefined);
  }

  private _checkNeedCreate(key: string): void {
    const value = localStorage.getItem(key);

    if (undefined === this._data[key]) {
      this._data[key] = new BehaviorSubject<string | undefined>(value ?? undefined);
    }
  }
}
