import { Observable } from 'rxjs';

export interface LocalDataRepository {
  get(key: string): Observable<string | undefined>;
  set(key: string, data?: string): void;
  remove(key: string): void;
  isSupported(): boolean;
}
